// SOIL_Issue24.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <string>

#include <windows.h>
#include <wingdi.h>
#include "GL/gl.h"

#include "SOIL2.h"

int main()
{
	auto getExeDir = []
	{
		char result[MAX_PATH];
		auto path = std::string(result, GetModuleFileNameA(nullptr, result, MAX_PATH));
		return path.substr(0, path.rfind("\\"));
	};

    std::string texFilePath = getExeDir() + "\\..\\SpartanJ-soil2-1c5848213798\\bin\\img_test.bmp";
    unsigned int texId = SOIL_load_OGL_texture(texFilePath.c_str(), SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	if (texId == 0)
	{
		std::cout << "ERROR: unable to load texture: " + texFilePath << std::endl;
		exit(EXIT_FAILURE);
	}

	std::cout << "Texture " << texFilePath << "loaded successfully" << std::endl;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
